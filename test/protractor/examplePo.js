'use strict';

var ExamplePO = function() {
    this.container = element(by.tagName('ul'));
}

ExamplePO.prototype = Object.create({}, {
    numberOfItems: {
        value: function () {
            return this.container.all(by.repeater('item in vm.items')).count();
        }
    }
});


module.exports = ExamplePO;