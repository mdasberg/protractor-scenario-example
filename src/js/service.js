(function () {
    'use strict';

    function Service($resource) {
        return $resource('/api/items', {}, {});
    }

    Service.$inject = ['$resource'];

    angular.module('example').factory('service', Service);

})();