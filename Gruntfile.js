'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    grunt.initConfig({
        config: {
            hosts: {
                runtime: 'localhost'
            },
            paths: {
                tmp: '.tmp',
                base: require('path').resolve(),
                src: 'src',
                test: 'test',
                results: 'results',
                instrumented: 'instrumented',
                config: 'config'
            }
        },
        clean: {
            files: [
                '<%=config.paths.tmp%>',
                '<%=config.paths.results%>',
                '<%=config.paths.instrumented%>',
            ]
        },
        jshint: {
            options: {
                jshintrc: '<%=config.paths.config%>/.jshintrc',
                reporter: require('jshint-junit-reporter'),
                reporterOutput: '<%=config.paths.results%>/jshint/jshint.xml'
            },
            files: {
                src: ['<%=config.paths.src%>/**/*.js']
            }
        },
        instrument: {
            files: '<%=config.paths.src%>/**/*.js',
            options: {
                basePath: '<%=config.paths.instrumented%>',
                lazy: false
            }
        },
        portPick: {
            options: {
                port: 9000
            },
            protractor: {
                targets: [
                    'connect.options.port'
                ]
            }
        },
        connect: {
            options: {
                port: 0,
                hostname: '0.0.0.0'
            },
            runtime: {
                options: {
                    open: {
                        target: 'http://<%= config.hosts.runtime%>:<%= connect.options.port%>'
                    },
                    middleware: function (connect) {
                        var config = grunt.config.get('config');

                        return [
                            connect().use('/node_modules', connect.static('./node_modules')),
                            connect().use('/', connect.static(config.paths.src))
                        ];
                    }
                }
            },
            test: {
                options: {
                    open: false,
                    middleware: function (connect) {
                        var config = grunt.config.get('config');
                        return [
                            connect().use('/node_modules', connect.static('./node_modules')),
                            connect().use('/js', connect.static(config.paths.instrumented + '/' + config.paths.src + '/js')),
                            connect().use('/', connect.static(config.paths.src))
                        ];
                    }
                }
            }
        },
        protractor_coverage: {
            example: {
                options: {
                    configFile: 'config/protractor.conf.js',
                    keepAlive: true,
                    noColor: false,
                    debug: false,
                    coverageDir: '<%=config.paths.results%>/protractor-coverage',
                    args: {
                        baseUrl: 'http://<%= config.hosts.runtime %>:<%= connect.test.options.port %>',
                        resultsDir: '<%=config.paths.results%>/protractor',
                        specs: ['<%=config.paths.test%>/protractor/*Spec.js']
                    }
                }
            }
        },
        makeReport: {
            src: '<%=config.paths.results%>/protractor-coverage/*.json',
            options: {
                type: 'lcov',
                dir: '<%=config.paths.results%>/protractor-coverage',
                print: 'detail'
            }
        },
        watch: {
            js: {
                files: ['<%=config.paths.src%>/{,*/}*.js']
            }
        }
    });


    grunt.registerTask('serve', 'Serve the app with runtime watches.', [
        'prepare',
        'connect:runtime',
        'watch'
    ]);

    grunt.registerTask('prepare', 'Prepare the build with all the necessary stuff.', [
        'clean',
        'portPick'
    ]);

    grunt.registerTask('test', 'Execute tests.', [
        'force:on',
        'jshint',
        'instrument',
        'connect:test',
        'protractor_coverage',
        'makeReport',
        'force:reset'
    ]);

    grunt.registerTask('default', 'Default task', [
        'prepare',
        'test'
    ]);
};
